# frozen_string_literal: true

# Copyright 2018 Richard Davis
#
# This file is part of vale.
#
# vale is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# vale is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vale.  If not, see <http://www.gnu.org/licenses/>.


module Vale
  ##
  # = Hello
  # Author::    Richard Davis
  # Copyright:: Copyright 2018 Richard Davis
  # License::   GNU Public License 3
  #
  # Contains modularized code for project; given example provides a greeting
  class Hello
    ##
    # Returns a greeting provided a name.
    def self.greeting(name)
      "Hello, #{name}."
    end
  end
end
